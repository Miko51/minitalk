[![tr](https://img.shields.io/badge/turkish-red?label=language)](README.tr.md) [![en](https://img.shields.io/badge/english-white?label=language)](README.md) 
# Minitalk
**The project aims to learn about signals and achieve communication using signals.**

## Usage
After cloning and running the 'make' command, you can execute the client and server programs. First, run the server program, then provide the PID number displayed on the screen as a parameter to the client program, followed by the message you want to send to the server as the last parameter.
![](./img/screenshot.png)

![](./img/screenshot2.png)

![](./img/screenshot3.png)

## Bonus
**In the bonus section, you should be able to print Unicode characters and an acknowledgment signal is required for the signals you send to the server.** 

## Score 
125/125 [Project subject](en.subject.pdf)

## Sources
[This](https://youtu.be/SxkMhvxecPE?list=PL5tDlMcZytRqvYbDWLoayxAkUcHsOxX_p&t=2788) will enable you to learn about signals. <br/>
[This](https://www.youtube.com/watch?v=jVnv7Nc5arE) video will teach you about Unicode logic.
