[![tr](https://img.shields.io/badge/turkish-red?label=language)](README.tr.md) [![en](https://img.shields.io/badge/english-white?label=language)](README.md) 
# Minitalk
**Projede sinyaller öğrenilir. Sinyaller ile haberleşme hedeflenir.**

## Kullanım
Klonlanıp make komutu çalıştırıldıktan sonra client ve server programını çalıştırabilirsiniz. İlk önce server programını çalıştırın ardından ekranda gözüken pid numarasını client programına parametre olarak verin ve son parametre olarak da servere göndermek istediğiniz mesaj.

![](./img/screenshot.png)

![](./img/screenshot2.png)

![](./img/screenshot3.png)

## Bonus
**Bonus kısımda unicode karakterler bastırabilmeniz ve servere yolladığınız sinyaller için onay sinyali istenir.** 

## Not 
Proje %125 ile geçmektedir. [Projenin detaylı yönergesi](tr.subject.pdf)

## Faydalanabilinecek Kaynaklar
[Bu](https://youtu.be/SxkMhvxecPE?list=PL5tDlMcZytRqvYbDWLoayxAkUcHsOxX_p&t=2788) video sinyalleri öğrenmenizi sağlayacaktır.
[Bu](https://www.youtube.com/watch?v=jVnv7Nc5arE) video unicode mantığını öğretecektir.
